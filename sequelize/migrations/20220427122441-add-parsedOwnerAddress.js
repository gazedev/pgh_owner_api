'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Properties', 'parsedOwnerAddress', {
      type: Sequelize.JSONB,
    });
  },
  down: async (queryInterface) => {
    await queryInterface.removeColumn('Properties', 'parsedOwnerAddress');
  }
};
