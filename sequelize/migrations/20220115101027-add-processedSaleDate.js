'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Properties', 'processedSaleDate', {
      type: Sequelize.DATE,
    });
  },
  down: async (queryInterface, Sequelize) => {
   
  }
};
