'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Properties', 'parsedTaxAddress', {
      type: Sequelize.JSONB,
    });
  },
  down: async (queryInterface) => {
    await queryInterface.removeColumn('Properties', 'parsedTaxAddress');
  }
};
