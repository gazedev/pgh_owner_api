module.exports = {
  routes: async (models) => {
    const controllers = await require('./properties.controllers.js')(models);
    const propertiesModels = require('./properties.models');
    const helpers = require('../helpers')(models);
    return [
      {
        method: 'GET',
        path: '/properties',
        handler: controllers.getProperties,
        options: {
          auth: 'jwt',
          description: 'Get properties',
          notes: 'Get properties for an inputted search query',
          tags: ['api', 'Properties'],
          validate: {
            query: propertiesModels.apiFilterQuery,
          }
        }
      },
      {
        method: 'GET',
        path: '/tcvcog/parcels',
        handler: controllers.getPropertiesForTCVCOG,
        options: {
          auth: 'jwt',
          description: 'Get properties by sale date and service area',
          notes: 'Get properties for an inputted date and service area',
          tags: ['api', 'Properties'],
          validate: {
            query: propertiesModels.apiTCVCOG,
          }
        }
      },
      {
        method: 'PUT',
        path: '/properties/upload/xlsx',
        options: {
          pre: [
            { method: helpers.ensureAdmin, failAction: 'error' },
          ],
          handler: controllers.convertXLSX,
          auth: 'jwt',
          description: 'Upload Updated .xlsx to convert',
          notes: 'Convert .xlsx to .csv and Upload',
          tags: ['api', 'Properties'],
          plugins: {
            'hapi-swagger': {
              payloadType: 'form',
            }
          },
          payload: {
            maxBytes: 524288000, // This may need to be set higher in the future?
            parse: true,
            output: 'file',
            multipart: true,
            timeout: 120000,
          },        
          validate: {
            payload: propertiesModels.xlsxFile,
          }
        }
      },
      {
        method: 'POST',
        path: '/properties/hook/csv',
        handler: controllers.uploadConvertedCSV,
        options: {
          pre: [
            { method: helpers.ensureConvertSecret, failAction: 'error' },      
          ],
          description: 'Upload XLSX Converted to CSV',
          notes: 'Delete All Properties then Upload CSV',
          tags: ['api', 'Properties'],

        }
      },
      {
        method: 'PUT',
        path: '/properties/upload/csv',
        options: {
          pre: [
            { method: helpers.ensureAdmin, failAction: 'error' },
          ],
          handler: controllers.uploadCSV,
          auth: 'jwt',
          description: 'Upload Updated CSV',
          notes: 'Delete All Properties then Upload CSV',
          tags: ['api', 'Properties'],
          plugins: {
            'hapi-swagger': {
              payloadType: 'form',
            }
          },
          payload: {
            maxBytes: 524288000, // This may need to be set higher in the future?
            parse: true,
            output: 'file',
            multipart: true,
            timeout: 120000,
          },        
          validate: {
            payload: propertiesModels.csvFile,
          }
        }
      },
    ];
  },
};
