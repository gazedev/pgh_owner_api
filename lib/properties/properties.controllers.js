module.exports = async (models) => {
  const Boom = require('@hapi/boom');
  const Sequelize = require('sequelize');
  const sequelize = models.sequelize;
  const Op = Sequelize.Op;
  const addressHelpers = require('../address/address.helpers');
  const fs = require('fs')

// const used = process.memoryUsage().heapUsed / 1024 / 1024;
// console.log(`The script uses approximately ${Math.round(used * 100) / 100} MB`);
  return {
    getProperties: async function(request, h) {
      for (let type of ['parcelId', 'address', 'owner', 'ownerAddress']) {
        if (propOf(request.query, type)) {
          request.query[type] = [].concat(request.query[type]);
        }
      }
      console.log('Request params:', JSON.stringify(request.query));
      let response;
      try {
        response = await getProperties(request.query);
      } catch (e) {
        throw Boom.badImplementation('Error during getProperties(request.query).', e);
      }
      return {
        query: request.query,
        count: response.length,
        results: response,
      };
    },
    getPropertiesForTCVCOG: async function(request, h) {
      console.log('Request params:', JSON.stringify(request.query));

      let response;
      try {
        response = await getPropertiesForTCVCOG(request.query['after'], request.query['interestAreas'], request.query['page'], request.query['limit']);
      } catch (e) {
        throw Boom.badImplementation('Error during getPropertiesForTCVCOG(request.query).', e);
      }

      return response;
    },
    uploadCSV: async function(request, h) {
      const csvFile = request.payload;      

      return uploadCSV(csvFile);
    },
    convertXLSX: async function(request, h) {      
      // This will send the file to owner_convert api convert it to csv. 
      // owner_convert will alert owner_api when file is saved.
      // That alert calls UploadConvertedCSV()

      // get the xlsx file
      const file = request.payload.file;
      
      // Make sure file is xlsx
      const headers = file.headers;
      const contentType = headers['content-type'];
      
      // NOTE:
      // Currently the file is still being saved to tmp first. 
      // Is there a way to check file type before import?
      // I think I looked at Joi Validation, and didn't find an option?      
      if(contentType !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
        throw Boom.badData('Error: File must be xlsx')
      }
      
      // Get dependencies and set boundary to obscure value.
      require('url');
      const http = require('http')
      const boundary = 'XXXXX'

      // Read file and send it to owner_convert for conversion to csv
      fs.readFile(file.path, function (err, content) {
        if (err) {
            console.log(err);
            return
        }
    
        let data = "";
        data += "--" + boundary + "\r\n";
        data += "Content-Disposition: form-data; name=\"file\"; filename=\"" + file.filename + "\"\r\nContent-Type: application/json\r\n";
        data += "Content-Type:application/octet-stream\r\n\r\n";
    
        const payload = Buffer.concat([
            Buffer.from(data, "utf8"),
            Buffer.from(content, 'binary'),
            Buffer.from("\r\n--" + boundary + "--\r\n", "utf8"),
        ]);
    
        const options = {
            host: "owner_convert",
            port: 5000,
            path: "/upload",
            method: 'PUT',
            headers: {
                "Content-Type": "multipart/form-data; boundary=" + boundary,
                "x-convert-token": process.env.CONVERT_SECRET,
            },
        }
    
        const chunks = [];
        const req = http.request(options, response => {
            response.on('data', (chunk) => chunks.push(chunk));
            response.on('end', () => console.log(Buffer.concat(chunks).toString()));
        });
    
        req.write(payload)
        req.end()
      })

      return {message: 'Successfully uploaded xlsx file to convert to csv...'}
    },
    uploadConvertedCSV: async function(request, h) {
      console.log("Uploading converted csv... ");

      if(!request.query.filename){
        throw Boom.badData('Must include query with filename!')
      }

      const filename = request.query.filename;
      // The owner_postgres has assesments directory mounted, so it has access to the file 
      // And owner_api has the whole repositiory mounted, so the filepaths are different within api and postgres
      const filepathInPostgres = '/assessments/' + filename;
      const filepathInApi = process.env.ASSESSMENTS_DIR_API + filename;

      let bytes;
      try {
        bytes = fs.statSync(filepathInApi, {encoding: 'utf8'}).size;
      } catch {
        throw Boom.badImplementation('Csv file not found! Check variables.env ASSESSMENTS_DIR_API.')
      }

      const csvFile = {
        file: {
          path: filepathInPostgres,
          bytes: bytes,
          filename: filename,
          headers: {
            'content-disposition': `form-data; name="file"; filename="${filename}"`,
            'content-type': 'text/csv'
          }
        }
      };

      uploadCSV(csvFile, filename);
      return {message: 'Converted csv found and is now uploading...'};
    },  
  };

  async function uploadCSV(csvFile, filename = null){
    console.log('Uploading .csv: ', csvFile);

    // Check to make sure csvFile is text/csv
    const headers = csvFile.file.headers;
    const contentType = headers['content-type'];

    // NOTE:
    // Currently the file is still being saved to tmp first. 
    // Is there a way to check file type before import?
    // I think I looked at Joi Validation, and didn't find an option?
    if(contentType !== 'text/csv'){
      throw Boom.badData('Error: File must be text/csv')
    }
    const t = await models.Property.sequelize.transaction();

    try {
      console.log('Destroying DB...'),
      await models.Property.destroy({
        where: {},
      }, {transaction: t})        

      console.log('Copying From CSV...');
      await copyFromCSV(sequelize, csvFile.file, t);

      console.log('Commiting Transaction...');
      await t.commit();
      
    } catch {        
      await t.rollback();
      throw Boom.badImplementation('Error during csv copying process. Rolled Back.');
    }

    console.log('Processing data...')
    console.time('Data Processing Time');
    try {
      await processData()
    } catch (err) {
      console.log(err);
      throw Boom.badImplementation('Error during processData.');
    }
    console.log('Done processing data.');
    console.timeEnd('Data Processing Time');
    
    // If csv was uploaded via conversion, delete csv and xlsx files
    if(filename){
      console.log('Removing files from assessments directory...');
      
      fs.unlinkSync(process.env.ASSESSMENTS_DIR_API + filename);
      console.log('Deleted ' + filename);
      
      const filenameXLSX = filename.replace('.csv', '.xlsx')
      fs.unlinkSync(process.env.ASSESSMENTS_DIR_API + filenameXLSX)
      console.log('Deleted ' + filenameXLSX);
    }

    console.log('Copied and processed .csv successfully!');
    return true;
  };
  async function copyFromCSV(sequelize, csvFile, t) {
    // We read the file in as LATIN1, since there are some characters that cause issues when read in as UTF8. If we wanted to drop non-UTF8 characters at the command line level, we could do:
    // iconv -c -t utf8 file_in.csv > file_out.csv

    // let csvFilePath = '/var/lib/postgresql/import/truncated.csv';
    const [results, metadata] = await sequelize.query(`COPY "Properties"("PARID", "PROPERTYOWNER", "PROPERTYHOUSENUM", "PROPERTYFRACTION", "PROPERTYADDRESS", "PROPERTYCITY", "PROPERTYSTATE", "PROPERTYUNIT", "PROPERTYZIP", "MUNICODE", "MUNIDESC", "SCHOOLCODE", "SCHOOLDESC", "LEGAL1", "LEGAL2", "LEGAL3", "NEIGHCODE", "NEIGHDESC", "TAXCODE", "TAXDESC", "TAXSUBCODE", "TAXSUBCODE_DESC", "OWNERCODE", "OWNERDESC", "CLASS", "CLASSDESC", "USECODE", "USEDESC", "LOTAREA", "HOMESTEADFLAG", "FARMSTEADFLAG", "CLEANGREEN", "ABATEMENTFLAG", "RECORDDATE", "SALEDATE", "SALEPRICE", "SALECODE", "SALEDESC", "DEEDBOOK", "DEEDPAGE", "PREVSALEDATE", "PREVSALEPRICE", "PREVSALEDATE2", "PREVSALEPRICE2", "AGENT", "TAXFULLADDRESS1", "TAXFULLADDRESS2", "TAXFULLADDRESS3", "TAXFULLADDRESS4", "CHANGENOTICEADDRESS1", "CHANGENOTICEADDRESS2", "CHANGENOTICEADDRESS3", "CHANGENOTICEADDRESS4", "CHANGENOTICETYPE", "CHANGENOTICENUMBER", "CHANGENOTICEFRACTION", "CHANGENOTICEDIRECTION", "CHANGENOTICESTREET", "CHANGENOTICESUFFIX", "CHANGENOTICESUFFIX2", "CHANGENOTICECITY", "CHANGENOTICESTATE", "CHANGENOTICECOUNTRY", "CHANGENOTICEPOSTALCODE", "CHANGENOTICEUNITDESC", "CHANGENOTICEUNITNUMBER", "CHANGENOTICEADDR1", "CHANGENOTICEADDR2", "CHANGENOTICEADDR3", "CHANGENOTICEZIP5", "CHANGENOTICEZIP4", "COUNTYBUILDING", "COUNTYLAND", "COUNTYTOTAL", "COUNTYEXEMPTBLDG", "LOCALBUILDING", "LOCALLAND", "LOCALTOTAL", "FAIRMARKETBUILDING", "FAIRMARKETLAND", "FAIRMARKETTOTAL", "STYLE", "STYLEDESC", "STORIES", "YEARBLT", "EXTERIORFINISH", "EXTFINISH_DESC", "ROOF", "ROOFDESC", "BASEMENT", "BASEMENTDESC", "GRADE", "GRADEDESC", "CONDITION", "CONDITIONDESC", "CDU", "CDUDESC", "TOTALROOMS", "BEDROOMS", "FULLBATHS", "HALFBATHS", "HEATINGCOOLING", "HEATINGCOOLINGDESC", "FIREPLACES", "BSMTGARAGE", "FINISHEDLIVINGAREA", "CARDNUMBER", "ALT_ID", "TAXYEAR", "ASOFDATE") FROM '${csvFile.path}' DELIMITER '\t' CSV HEADER encoding 'LATIN1';`, {transaction: t});

    console.log('csv results', results, metadata);
  }
  
  async function processData() {
    let limit = 1000;
    let offset = 0;
    let instances;
    do {
      instances = await models.Property.findAll({
        where: {
          [Op.or]: {
            address: null,
            searchableAddressV1: null,
            ownerAddress: null,
            searchableOwnerAddressV1: null,
            processedSaleDate: null,
            parsedTaxAddress: null,
            parsedOwnerAddress: null,
          }
        },
        attributes: [
          'PARID',
          'PROPERTYHOUSENUM',
          'PROPERTYADDRESS',
          'PROPERTYCITY',
          'PROPERTYSTATE',
          'PROPERTYZIP',
          'PROPERTYOWNER',
          'CHANGENOTICEADDRESS1',
          'CHANGENOTICEADDRESS2',
          'CHANGENOTICEADDRESS3',
          'CHANGENOTICEADDRESS4',
          'SALEDATE',
          'TAXFULLADDRESS1',
          'TAXFULLADDRESS2',
          'TAXFULLADDRESS3',
          'TAXFULLADDRESS4',
        ],
        offset: 0,
        limit: limit,
      });

      console.log("Instance Count:", instances.length);
      if (instances.length > 0) {
        console.log(`Processing - limit: ${limit}. offset: ${offset}. instances: ${instances.length}`);
      }
      let saves = [];
      offset = offset + limit;
      for (var i = 0; i < instances.length; i++) {
        let instance = instances[i];
        let address = `${instance['PROPERTYHOUSENUM']} ${instance['PROPERTYADDRESS']} ${instance['PROPERTYCITY']} ${instance['PROPERTYSTATE']} ${instance['PROPERTYZIP']}`;
        let addressV1 = addressHelpers.preProcessAddress(address);

        let owner = instance['PROPERTYOWNER'].replace(/\s+/g, ' ');
        let ownerAddress = `${instance['CHANGENOTICEADDRESS1']} ${instance['CHANGENOTICEADDRESS2']} ${instance['CHANGENOTICEADDRESS3']} ${instance['CHANGENOTICEADDRESS4']}`;
        let ownerAddressV1 = addressHelpers.preProcessAddress(ownerAddress);

        let processedSaleDate = new Date(instance['SALEDATE']);
        
        let parsedOwnerAddress = addressHelpers.parseAddress(ownerAddress);
        if(!parsedOwnerAddress){
          throw Boom.badImplementation('Error parsing ownerAddress: ' + ownerAddress);
        }
        parsedOwnerAddress.addressee = owner;
        
        let taxAddress = `${instance['TAXFULLADDRESS1']} ${instance['TAXFULLADDRESS3']} ${instance['TAXFULLADDRESS4']}`;
        let parsedTaxAddress = addressHelpers.parseAddress(taxAddress);
        if(!parsedTaxAddress){
          throw Boom.badImplementation('Error parsing taxAddress: ' + taxAddress);
        }
        parsedTaxAddress.addressee = owner;
        let secUnitDetail = instance['TAXFULLADDRESS2'].trim();
        if(secUnitDetail){
          parsedTaxAddress.sec_unit_detail = secUnitDetail;
        }

        instance['owner'] = owner;
        instance['address'] = address.replace(/\s+/g, ' ');
        instance['searchableAddressV1'] = addressV1;
        instance['ownerAddress'] = ownerAddress.replace(/\s+/g, ' ');
        instance['searchableOwnerAddressV1'] = ownerAddressV1;
        instance['processedSaleDate'] = processedSaleDate;
        instance['parsedTaxAddress'] = parsedTaxAddress;
        instance['parsedOwnerAddress'] = parsedOwnerAddress;
        let save = instance.save({
          fields: ['address', 'searchableAddressV1', 'owner', 'ownerAddress', 'searchableOwnerAddressV1', 'processedSaleDate', 'parsedTaxAddress', 'parsedOwnerAddress'],
        });
        saves.push(save)
      }
      await Promise.all(saves);

    } while (instances.length !== 0);
    return true;
  }

  async function getProperties(queries = null) {
    let whereOptions = {};
    if (queries !== null) {
      whereOptions[Op.or] = {};
    }

    if (propOf(queries, 'parcelId')) {
      whereOptions[Op.or].PARID = {
        [Op.or]: queries.parcelId.map((val) => {
          return {[Op.iLike]: '%' + val + '%'};
        }),
      };
    }
    if (propOf(queries, 'address')) {
      whereOptions[Op.or].searchableAddressV1 = {
        [Op.or]: queries.address.map((val) => {
          return {[Op.iLike]: '%' + addressHelpers.preProcessAddress(val, replacePercent = false) + '%'}
        }),
      };
    }
    if (propOf(queries, 'owner')) {
      whereOptions[Op.or].owner = {
        [Op.or]: queries.owner.map((val) => {
          return {[Op.iLike]: '%' + val + '%'};
        }),
      };
    }
    if (propOf(queries, 'ownerAddress')) {
      whereOptions[Op.or].searchableOwnerAddressV1 = {
        [Op.or]: queries.ownerAddress.map((val) => {
          return {[Op.iLike]: '%' + addressHelpers.preProcessAddress(val, replacePercent = false) + '%'}
        }),
      };
    }

    let properties = await models.Property.findAll({
      where: whereOptions,
      attributes: [
        ['PARID', 'parid'],
        'address',
        ['MUNIDESC', 'muniDesc'],
        ['PROPERTYOWNER', 'owner'],
        'ownerAddress',
        ['HOMESTEADFLAG', 'home'],
        "GRADE",
        "GRADEDESC",
        "CONDITION",
        "CONDITIONDESC",
        "ASOFDATE",
      ]
    });

    return properties;
  }

  async function getPropertiesForTCVCOG(saleDate = null, muniCodes = null, page = 0, limit = undefined){
    let whereParams = {};
    if(saleDate){
      whereParams.processedSaleDate = {
        [Op.between]: [saleDate, Date.now()]
      }
    }
    
    if(muniCodes){
      whereParams.MUNICODE = {
        [Op.in]: muniCodes,
      }      
    }
    
    // By default, just search with whereParams and optional offset.
    let findAllParams = { 
      where: whereParams,
      offset: page 
    };
    // If a limit is passed in, add limit and modify offset based on that limit.
    if(limit){
      findAllParams.limit = limit;
      findAllParams.offset = limit * page;
    }
    
    const properties = await models.Property.findAll(findAllParams);

    return properties;
  }

  function propOf(obj, prop) {
    return Object.prototype.hasOwnProperty.call(obj, prop);
  }

};
