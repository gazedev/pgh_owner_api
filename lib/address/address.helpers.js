module.exports = {
  preProcessAddress: preProcessAddress,
  parseAddress: parseAddress,
};

function preProcessAddress(address, replacePercent = true, joiner = '') {
  // we want to be careful not to convert % on search strings
  // because that can be used to assist in LIKE searching
  const specialChars = /[/+-.,!?@#$^&*<>;:|]/gi;
  const percent = /[%]/gi;
  const multipleSpaces = /\s+/g;
  address = address.replace(specialChars, ' ');
  if (replacePercent) {
    address = address.replace(percent, ' ');
  }
  // reduce any occurences of multiple spaces with just one
  address = address.replace(multipleSpaces, ' ');
  // split address apart on spaces so we can transform each part individually
  let parts = address.split(' ');
  let processedParts = [];
  for (let part of parts) {
    part = part.toUpperCase();
    part = ordsConvertPart(part);
    part = termConvertPart(part);
    processedParts.push(part);
  }

  // There may be an issue caused by a zip code starting in 0.
  // Somewhere in the assessments data collection process, the leading 0 gets removed, 
  // resulting in a 4 digit zip code.
  
  // We get the zip, which should be the last word in the address.
  let lastWord = processedParts[processedParts.length - 1];
  
  // Attempt to validate 4 digit zipcode 
  if(lastWord.length == 4 && /^\d+$/.test(lastWord)){
    // console.log('Issue with zip code: ', lastWord);
    // Prepend '0' to the zip code
    lastWord = '0'.concat(lastWord);
    processedParts[processedParts.length - 1] = lastWord ;
  }
  
  let processedAddress = processedParts.join(joiner);

  return processedAddress;
}

function parseAddress(address){
  const Parser = require('parse-address');

  let processedAddress = preProcessAddress(address, replacePercent = true, join = ' ');
  let parsedAddress = Parser.parseLocation(processedAddress);

  // If the address fails to parse it returns null:
  if(!parsedAddress){
    console.log('Unresolved Parsing Issue', processedAddress);
  }

  return parsedAddress;
}

function ordsConvertPart(part) {
  const ORDS = require('./terms.js').ORDS;
  if (!ORDS.hasOwnProperty(part)) {
    return part;
  }
  return ORDS[part];
}

function termConvertPart(part) {
  const TERMS = require('./terms.js').TERMS;
  if (!TERMS.hasOwnProperty(part)) {
    return part;
  }
  return TERMS[part];
}
