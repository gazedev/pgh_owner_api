from xlsx2csv import Xlsx2csv

def convert_assessments(xlsx_filename): 

    csv_filename = xlsx_filename.rsplit('.xlsx', 1)[0] + '.csv'

    Xlsx2csv('./assessments/' + xlsx_filename, outputencoding="utf-8", delimiter='\t').convert('./assessments/' + csv_filename)

    return csv_filename
