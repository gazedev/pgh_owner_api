from flask import Flask
import os

UPLOAD_FOLDER = './assessments'
CONVERT_SECRET = os.environ['CONVERT_SECRET']

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['CONVERT_SECRET'] = CONVERT_SECRET
app.config['MAX_CONTENT_PATH'] = 524288000