import os, requests, threading
from app import app
import converter
from flask import request,  jsonify, send_file
from werkzeug.utils import secure_filename

ALLOWED_EXTENSIONS = set(['xlsx'])

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def index():
    return 'Make a POST request to /upload with .xlsx file to convert to .csv ...'
        
@app.route('/upload', methods=['PUT'])
def upload():
    if request.headers['x-convert-token'] != app.config['CONVERT_SECRET']:
        resp = jsonify({'message' : 'Access Denied!'})
        resp.status_code = 403
        return resp 
    
    if 'file' not in request.files:
        resp = jsonify({'message' : 'No file part in the request', 'files': request.files})
        resp.status_code = 400
        return resp
    
    file = request.files['file']
    
    if file.filename == '':
        resp = jsonify({'message' : 'No file selected for uploading'})
        resp.status_code = 400
        return resp

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

        # starts the conversion on a new thread so we can get a response 
        c = threading.Thread(target=convert, args=(filename,))
        c.start()

        resp = jsonify({'message' : 'xlsx file recieved. Converting to csv. This may take a while...'})
        resp.status_code = 201

        return resp

    else:
        resp = jsonify({'message' : 'Allowed file type is .xlsx'})
        resp.status_code = 400
        return resp

def convert(xlsx):
    csv_filename = converter.convert_assessments(xlsx)
    payload = {'filename': csv_filename}
    headers = {'x-convert-token': app.config['CONVERT_SECRET']}

    # Now we ping the owner_api that the file is ready
    resp = requests.post('http://owner_api:8081/properties/hook/csv', params=payload, headers=headers)

    return resp.text



# Todo: Do we need to delete the files after successful upload?
# We will need to clean up the files in the owner_api, after successful upload.

if __name__ == '__main__':
    app.run(host='0.0.0.0')  # run our Flask app